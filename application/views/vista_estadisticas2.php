   <div class="page-header">
     <h1>Historial <?=$campo[0]->nombre?></h1>
   </div>
   <p>
		<table class="table table-striped">
	        <thead>
	          <tr>
	            <th>Mes</th>
	            <th>Pozo</th>
	            <th>Días de Operación</th>
	            <th>Corrida</th>
	          </tr>	 
	        </thead>
	        <tbody>
	        	
				<?php for ($i=0; $i < count($tabla[0]); $i++) { ?>
		        	<tr>
		        		<td>
		        			<?=empty($tabla[0][$i]) ? 0 : $tabla[0][$i]?>
		        		</td>
		        		<td>
		        			<?=empty($tabla[1][$i][0]->nombre_pozo) ? 0 : $tabla[1][$i][0]->nombre_pozo?>
		        			
		        		</td>
		        		<td>
		        			<?=empty($tabla[1][$i][0]->dias_operacion) ? 0 : $tabla[1][$i][0]->dias_operacion?>
		        		</td>
		        		<td>
		        			<?=empty($tabla[1][$i][0]->corrida) ? 0 : $tabla[1][$i][0]->corrida?>
		        		</td>
		        	</tr>
				<?php } ?>
	          
	        </tbody>
		</table>

	    <div id="chart1" style="width:120%; height:500px"></div>
	    <!-- <div id="chart2" style="width:50%; height:500px"></div> -->
   </p>
 </div>
 